/*
    ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4)

    Copyright © 2015, 2022 me

    This is anti-capitalist software, released for free use by individuals and organizations that do not operate by capitalist principles.

    Permission is hereby granted, free of charge, to any person or organization (the "User") obtaining a copy of this software and associated documentation files (the "Software"), to use, copy, modify, merge, distribute, and/or sell copies of the Software, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in all copies or modified versions of the Software.

    2. The User is one of the following:
    a. An individual person, laboring for themselves
    b. A non-profit organization
    c. An educational institution
    d. An organization that seeks shared profit for all of its members, and allows non-members to set the cost of their labor

    3. If the User is an organization with owners, then all owners are workers and all workers are owners with equal equity and/or equal vote.

    4. If the User is an organization, then the User is not law enforcement or military, or working for or under either.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


    ======================================================================
    As a special exception, if you somehow find something in here that's useful to you in any way, shape or form, you may use the BSD0 License as stated below

    BSD Zero Clause License

    Copyright (c) 2015, 2022 Figdor32

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
*/
container = 'BODY';
// internal_endln = '';
// internal_suppressblock = false;
internal_isnest = false;
function mnusz(a, b)   {return a * b;}
function dziel(a, b)   {return a / b;}
function dojeb(a, b)   {return a + b;}
function ujeb (a, b)   {return a - b;}
function procenta(a, b)   {return a %= b;}
function poteguj()     {return "Spierdalaj";}
function teksta(a)     {return a;}
function betternull(){ return undefined; }
function element(tag, a, id, cl, para) { return c('<' + tag + ic(id, cl) + (para!=undefined?para:'') + '>' + (a!=undefined?a:'') + '</'+tag+'>'); }
function c(a) { if (a == undefined) { return $(container); } else { if (internal_isnest){ return a } else { c().innerHTML += a;}}}
function title(a) { huj = document.getElementsByTagName('TITLE'); if (huj.length == 0) { alert('zjebalo sie'); } else { if (!window.document.documentMode) { huj[0].innerText = a; }} }
function $(a){  x = document.querySelectorAll(a); if (x.length == 1) return x[0]; else return x; }
function suppressblock() {internal_suppressblock = true; }
function ic(id, cl){ return (id!=undefined?' id="'+id+'"':'')+(cl!=undefined?' class="'+cl+'"':''); }
function nest(a) {internal_isnest = true; x = a(); internal_isnest = false; return x; }
function style(a, key, value) { if (a.innerHTML == undefined){ Array.from(a).forEach(function(x) { x.style[key] = value; }); } else { a.style[key] = value; } }
function setcontainer(a) { if (a!=undefined) container = a; else container='BODY'; }
function header(a, b, id, cl) { if (b == undefined) b = 1; return c('<h'+b+''+ic(id, cl)+'>' + a + '</h'+b+'>'); }
function marquee(a, id, cl) { return c('<marquee'+ic(id, cl)+'>' + a + '</marquee>'); }
function hr(id, cl, para) { return c('<hr'+ic(id, cl)+(para!=undefined?para:'') + '/>'); }
function br(id, cl, para) { return c('<br'+ic(id, cl)+'/>'); }
function div(id, cl, a, para) { return element('div', a, id, cl, para); }
function p(a, id, cl, para) { return element('p', a, id, cl, para); }
function span(a, id, cl, para) { return element('span', a, id, cl, para); }
function b(a, id, cl, para) { return element('b', a, id, cl, para); }
function i(a, id, cl, para) { return element('i', a, id, cl, para); }
function u(a, id, cl, para) { return element('u', a, id, cl, para); }
function img(a, alt, id, cl, para) { return element('img', '', id, cl, (para!=undefined?para:'') + ' src="'+a+'"' + (alt!=undefined?' alt="'+alt+'"':'')); }
